# -*- coding: utf-8 -*-

import jwt
import sys
from flask import Flask, redirect, url_for
from flask import request
from flask import render_template

reload(sys)
sys.setdefaultencoding('utf8')

class User:
    def __init__(self, user_id, email):
        self.user_id = user_id
        self.email = email


redirect_url = 'http://192.169.2.19:33200/login?page=http://localhost:8080/'
users = {}
# 加载pub.key私钥文件
pub_key = open("./pub.key").read()

app = Flask(__name__)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


@app.route('/', methods=["GET"])
def index():
    return render_template('index.html')


@app.route('/redirect_to_index')
def redirect_to_index():
    return redirect(url_for('index'))


@app.route('/login')
def login():
    return redirect(redirect_url, 302)


@app.route("/", methods=["POST"])
def auth():

    if request.form.has_key("token"):
        token = request.form["token"]
    else:
        message = "token 不能为空，请传入"
        return render_template('message.html', message=message)

    try:
        # 验证Token,并且在验证通过之后解析Payload，如果只是想解析Payload内容的话，可以将verify设置为false
        jwt.decode(token, pub_key, verify=True, issuer="iam", algorithms='RS256')
        if request.form.has_key("user_id"):
            user_id = request.form["user_id"]
        else:
            message = "user_id 不能为空，请传入"
            return render_template('message.html', message=message)

        if request.form.has_key("email"):
            email = request.form["email"]
        else:
            message = "email 不能为空，请传入"
            return render_template('message.html', message=message)

        if users.has_key(user_id):
            message = " 欢迎 , " + user_id
            return render_template('message.html', message=message)
        else:
            users[user_id] = User(user_id=user_id, email=email)
            message = " 登录成功，欢迎 " + users[user_id].user_id
            return render_template('message.html', message=message)
    except Exception as e:
        message = " 登录失败，原因 :" + e.message
        return render_template('message.html', message=message)
        #return repr(e), 401


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080)
