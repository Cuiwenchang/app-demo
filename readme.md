curl示例:
```bash
curl --request POST \
  --url http://192.169.2.15:15435 \
  --header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im9wZXJhdG9yQGxuaGwuY29tIiwiZXhwIjoxNjE2NDU2NjA0LCJpZCI6Im9wZXJhdG9yQGxuaGwuY29tIiwiaXNzIjoiaWFtIiwib3JpZ19pYXQiOjE1MjY0NTY2MDQsInVzZXJfaWQiOiJrdXNyLW9wZXJhdG9yIiwidXNlcl90eXBlIjoib3BlcmF0b3IifQ.hdzRAHub2IZH2ilDdTzbvJFg_ZWB7ELm7D4t4eMCzrSHiG9RK60yBCV7pfi5rltf4XkD2eWXAurlE4JNFzlLtw' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data 'user_id=kusr-12345678&email=test%40lnhl.com'
```
