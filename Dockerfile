FROM python:2.7-alpine

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN apk add --no-cache py-configobj libusb py-pip python-dev gcc linux-headers  musl-dev libffi-dev openssl-dev
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
EXPOSE 8080
CMD [ "python", "./main.py" ]
